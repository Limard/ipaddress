package IPAddress

import (
	"errors"
	"net"
	"strings"
)

var ErrNoValidInterface = errors.New(`no valid network devices`)
var ErrNoInterface = errors.New(`no network devices`)

var ignoreMAC = []string{
	//https://hwaddress.com/company/vmware-inc
	`00:05:69`,
	`00:0C:29`,
	`00:1C:14`,
	`00:50:56`,
}

func inIgnoreMAC(mac string) bool {
	for _, im := range ignoreMAC {
		if strings.HasPrefix(mac, im) {
			return true
		}
	}
	return false
}


func GetVaildInterface() (i []net.Interface, e error) {
	interfaces, e := net.Interfaces()
	if e != nil {
		return
	}
	if len(interfaces) == 0 {
		e = ErrNoInterface
		return
	}
	//buf, _ := json.Marshal(interfaces)
	//fmt.Println(string(buf))
	for _, inter := range interfaces {
		if inter.MTU == -1 {
			continue
		}
		if inter.Flags&net.FlagUp == 0 {
			continue
		}
		if inIgnoreMAC(inter.HardwareAddr.String()) {
			continue
		}
		i = append(i, inter)
	}
	if len(i) == 0 {
		e = ErrNoValidInterface
	}
	return
}
